# NATO phonetic alphabet

NATO phonetic alphabet, npa for short, will spell out any string you fire at it. On top of that, it will automatically save it as an audio file. It is spoken by gTTS, the [*Google Text-to-Speech*](https://pypi.org/project/gTTS/) module for Python.

## Example

"Hello World!" will return:

```python
"Message incoming Hotel Echo Lima Lima Oscar underscore Whiskey Oscar Romeo Lima Delta"
```

## Installation

First, clone the repo

Second, install the dependencies. They are listed in the [requirements.txt](requirements.txt)

```bash
pip install -r requirements.txt
```

## Usage

Run the main.py

```bash
python3 main.py
```

## Further notes

When I was searching for a project like this, I only found projects that only had the translation.

I later found out that there is a Ruby gem that does exactly this, the [NATO gem](https://github.com/lukelex/NATO) by [Lukas Alexandre](https://github.com/lukelex)

It is definably an awesome project to check out!!!

## References
* http://en.wikipedia.org/wiki/NATO_phonetic_alphabet

## License

[UNLICENSE](https://choosealicense.com/licenses/unlicense/)
