#! python3

# To write files and make a directory
import os

# To filter string
import re

# For sleeping
import time

# Googles Text to speech lib
from gtts import gTTS

# File info reader, in this case, it is for determening the length of a mp3
from mutagen.mp3 import MP3


def format_text(text: str):
    """
    Removes unwanted characters
    Replaces spaces with underscores
    """
    whitelist = re.compile(r'[^a-zA-Z ]+')
    return re.sub(whitelist, '', text).replace(' ', '_')


def create_dir(dir_name: str):
    """Creates directory when not made yet"""
    if not os.path.isdir(dir_name):
        os.makedirs(dir_name)


def create_spell_out(text: str):
    """Maps the characters to the NATO words"""
    words_for_letters = ['Alfa', 'Bravo', 'Charlie', 'Delta', 'Echo', 'Foxtrot',
                         'Golf', 'Hotel', 'India', 'Juliett', 'Kilo', 'Lima',
                         'Mike', 'November', 'Oscar', 'Papa', 'Quebec', 'Romeo',
                         'Sierra', 'Tango', 'Uniform', 'Victor', 'Whiskey',
                         'X-ray', 'Yankee', 'Zulu', 'underscore']

    # ord('{') - 97 == words_for_letters.index('underscore')
    words = [word.replace('_', '{') for word in text.split(' ')]

    return [words_for_letters[ord(
        letter.lower()) - 97] for word in words for letter in word]


def save_audio(text: str, filename: str, dir: str):
    """
    Converts text to audio and saves

    Notes
    -----
    If the .mp3 file extension is missing in the filename, it will be added
    If a file with the same name exists, it will not save, only notify the user

    Returns
    _______
    Path : str
    """

    # Make the path to the folder
    path = '{0}/{1}'.format(dir, filename)

    if not filename.endswith('.mp3'):
        path += '.mp3'

    # Generates and saves audio file
    tts = gTTS(text=text, lang='en')

    # Only saves when file does not exist
    if os.path.isfile(path):
        print("File named {0} already exist, will not safe".format(path))
    else:
        tts.save(path)
    return path


def play_audio(path: str):
    os.startfile(os.getcwd() + path[1:])
    duration = MP3(path).info.length
    time.sleep(duration)


# ---------- MAIN-PROGRAM ----------
if __name__ == '__main__':
    print('Welcome!')

    output_dir = './audio'
    create_dir(output_dir)

    while True:
        text = format_text(input('What is the sentence?'))

        output_words = 'Message incoming' + ', '.join(create_spell_out(text))

        path = save_audio(output_words, text, output_dir)
        play_audio(path)
        if(input('Continue? y/n') not in ('y', 'yes')):
            break
